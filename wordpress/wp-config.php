<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'IFTS-ProjectWork');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'menghi');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xpEb@2&kt*f2{,fcp-:zPd|sJ%OsI9 &p++,jQy+Rme|%n6s>`}=2!Jg$X|l07*a');
define('SECURE_AUTH_KEY',  '9faBO.@hp7x!T|f#lB)4@:N^alW2}n!vL{TXW82-wVX=Tp<{-RJQ|zAQ-pddAsnq');
define('LOGGED_IN_KEY',    'qKGGH0qu`]BDX9,~-z6j64>[N;ATQBOQEJIBN8&c{TEXgn1*@|Nm:8O%j27y-H{X');
define('NONCE_KEY',        ']}&-ZD/5045fi7@^,qFQ: qQ})r;%Fqq]/EACJ-ijSD;_5e$.uXJkeos?a4xC}=}');
define('AUTH_SALT',        '0bCf_GaA_#:d$9I$gU!MXV<Rz+mx#2v3P9|a*wi->f|A>^Oc^i`ZrkD&>}&kkE08');
define('SECURE_AUTH_SALT', 'EExaFYj`Av<5@PeRhv#.U}<l0LYP|t})=}ZCO`Z.?C8)$u2z7 B?xs,qRa=.K9]F');
define('LOGGED_IN_SALT',   'x[7B-~sO4LC:Y,@p-hTc!+wDHsvc5ANC(wvj8%+Kn#m{E+~bntc#qk~CSHM5e_QD');
define('NONCE_SALT',       'H.R<2wy|)OGz `v=SC[JhoD1c}1b}cL-r+:s<deqddo7R0yL`a_Uo856sr|`J-&O');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
