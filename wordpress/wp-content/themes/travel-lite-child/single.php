<?php

/* 	Travel Theme's Single Page to display Single Page or Post
	Copyright: 2012-2016, D5 Creation, www.d5creation.com
	Based on the Simplest D5 Framework for WordPress
	Since Travel 1.0
*/


get_header(); ?>

<div id="container">

<div id="content">
          
		  <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          
            <h1 class="page-title"><?php the_title(); ?></h1><div class="content-ver-sep fwtsep"></div>	
            <div class="entrytext"><div class="thumb"><?php the_post_thumbnail(); ?></div>
            <?php
            //Custom Post
            ?>
            <h4>Info:</h4>
            <?php
            $articolo_ID=get_the_id();
          	$apertura = get_post_meta($articolo_ID,'apertura',true);
          	$chiusura = get_post_meta($articolo_ID,'chiusura',true);
          	if($apertura != "" && $chiusura != ""){
          ?>
          <p><b>Periodo di apertura del parco:</b><?php echo "Dal " .DataCambio($apertura) ." Al " .DataCambio($chiusura);}
          	?>
          	<?php
          	$orario_apertura = get_post_meta($articolo_ID,'orario',true);
          	if($orario_apertura != ""){
          ?>
          <p><b>Orario di apertura del parco:</b><?php echo " $orario_apertura";}
          	?>
          	<?php
          	$orario_chiusura = get_post_meta($articolo_ID,'orario_chiusura',true);
          	if($orario_chiusura != ""){
          ?>
          <p><b>Orario di chiusura del parco:</b><?php echo " $orario_chiusura";}
          	?>
          	<?php
          	$prezzo_adulti = get_post_meta($articolo_ID,'prezzo_per_adulti',true);
          	if($prezzo_adulti != ""){
          ?>
          <p><b>Prezzo per adulti:</b><?php echo " $prezzo_adulti €";}
          	?>
          	<?php
          	$prezzo_ridotto = get_post_meta($articolo_ID,'prezzo_per_bambini',true);
          	if($prezzo_ridotto != ""){
          ?>
          <p><b>Prezzo ridotto:</b><?php echo " $prezzo_ridotto €";}
          	?>
          	<?php
          	$disabili = get_post_meta($articolo_ID,'disabili',true);
          	if($disabili != ""){
          ?>
          <p><b>Accessibilità ai disabili:</b>
          			<?php 
						if($disabili == "Si"){
							?>
							<span class="glyphicon glyphicon-ok" style="color:green"></span>
							<?php
						}else{
							?>
							<span class="glyphicon glyphicon-remove" style="color:red"></span>
							<?php
						}	          			
          }
          			?>
          <?php
          	$indirizzo = get_post_meta($articolo_ID,'indirizzo',true);
          	if($indirizzo != ""){
          ?>
          <p><b>Indirizzo:</b><?php echo $indirizzo['address'];
				echo do_shortcode('[pw_map address="' .$indirizzo['address'] .'" height="200px" key="AIzaSyCgKkTJD9GlWoB_PvX_d7glKbHAqpNCTYk"]');             
          }
          	?>
          	<br>
          	<?php
          	$sito = get_post_meta($articolo_ID,'sito_ufficiale',true);
          	if($sito != ""){
          ?>
          <p><b>Sito ufficiale:</b><a href="<?php echo"$sito";?>"> <?php echo"$sito"; ?> </a> <?php }
          	?>
          	<br>
          	<h4>Descrizione:</h4>
			<?php the_content(); ?>
            </div>
            <div class="clear"> </div>
            <?php  wp_link_pages( array( 'before' => '<div class="page-link"><span>' . 'Pages:' . '</span>', 'after' => '</div>' ) ); ?>
            <div class="up-bottom-border">
            <div class="content-ver-sep"></div>
          	</div>
			 <div class="content-ver-sep"></div><br />
			<?php endwhile;?>
          <hr>
          <?php
				echo do_shortcode('[ssba-buttons]');          
          ?>     
          <!-- End the Loop. -->          
</div>			
<?php get_sidebar(); ?>
<?php get_footer(); ?>