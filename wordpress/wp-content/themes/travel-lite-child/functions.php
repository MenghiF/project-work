<?php
	add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style' );
	function enqueue_parent_theme_style() {
		wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
	}
	
//Funzione per cambiare la visualizzazione della data
function DataCambio($abc)
{
    $anno=substr($abc,0,4);
    $mese=substr($abc,4,2);
    $giorno=substr($abc,6,2);
    $data=$giorno."-".$mese."-".$anno;
    return $data;
}