<?php
/*
	Travel Theme's Front Page to Display the Home Page if Selected
	Copyright: 2012-2016, D5 Creation, www.d5creation.com
	Based on the Simplest D5 Framework for WordPress
	Since Travel 1.0
*/
?>

<?php get_header(); ?>

<div id="container">
<?php
if(!empty($_GET['msg'])){
	?>
	<div class="alert alert-success">
  	<?php
  	echo $_GET['msg'];
  	?>
	</div>
<?php
}

?>

<div id="container">
<form action="./wp-content/themes/travel-lite-child/mail.php" method="POST">
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<div class="parchi" >
<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
 <h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>	
 <div class="entrytext"><div class="thumb"><?php the_post_thumbnail(); ?></div>
 <?php the_excerpt(); ?>
 <div class="clear"> </div>
 </div>
 </div>
 <label>Clicca qui per ricevere informazioni: </label>
 <input type="checkbox" class ="form-check" name="parchi[]" value="<?php the_title() ?>" onclick="myFunction()">
 </div>
 <?php endwhile; ?>
 <div id="page-nav">
	<div class="alignleft"><?php previous_posts_link(__('&laquo; Previous Entries','travel-lite')) ?></div>
	<div class="alignright"><?php next_posts_link(__('Next Entries &raquo;','travel-lite'),'') ?></div>
	</div>
	<br><br>
<h5>Parchi Selezionati:</h5>
<button id="parchi-selezionati" name="selezionati"></button>
<div id="parchi-selezionati"><br></div>
<label>Data Arrivo</label>
<input type="date" class="form-control" name="Arrivo" required><br>
<label>Data Partenza</label>
<input type="date" class="form-control" name="Partenza" required><br>
<label>Bambini</label>
<input type="number" class="form-control" min="0" name="Bambini"><br>
<label>Adulti</label>
<input type="number" class="form-control" min="0" name="Adulti"><br>
<label>E-mail</label>
<input type="email" class="form-control" placeholder="E-mail" name="mail">
<br>
<input type="submit" class="btn btn-info form-control" value="Chiedi informazioni per i parchi selezionati">
	</form>

 <?php else: ?>
 
 		<h1 class="arc-post-title"><?php _e('Sorry, we could not find anything that matched...', 'travel-lite'); ?></h1>
		<h3 class="arc-src"><span><?php _e('You Can Try the Search...', 'travel-lite'); ?></span></h3>
		<?php get_search_form(); ?><br />
		<p><a href="<?php echo home_url(); ?>" title="<?php _e('Browse the Home Page', 'travel-lite'); ?>">&laquo; <?php _e('Or Return to the Home Page', 'travel-lite'); ?></a></p><br />
		 
<?php endif; ?>
<div class="content-ver-sep"></div>
</div>
<?php get_footer(); ?>

<script type="text/javascript" >
function myFunction() {
  // Get the checkbox
  var checkBox = document.getElementsByName("parchi[]");
  var parchi = [];
  for(i=0; i<checkBox.length; i++){ 
	if(checkBox[i].checked){
	parchi.push(checkBox[i].value);
	console.log(parchi);
	document.getElementById("parchi-selezionati").innerHTML = "";
  }
}
	for(i=0; i<parchi.length; i++){
		if(parchi[i] != "undefined")
	document.getElementById("parchi-selezionati").innerHTML += parchi[i];
	document.getElementById("parchi-selezionati").innerHTML += "," ;	
	}
}
</script>